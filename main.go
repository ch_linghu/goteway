package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"path/filepath"
	"strings"
)

type Static struct {
	Url  string
	Path string
}

type Proxy struct {
	Url     string
	Backend string
}

type Config struct {
	Addr    string
	Port    int
	Statics []Static
	Proxies []Proxy
}

func NewConfig() *Config {
	conf := new(Config)
	conf.Statics = make([]Static, 0)
	conf.Proxies = make([]Proxy, 0)

	return conf
}

func LoadConfig(filename string) (conf Config, err error) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return Config{}, err
	}

	err = json.Unmarshal(content, &conf)
	if err != nil {
		return Config{}, err
	}

	return
}

func genHandler(conf *Config) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		url := r.URL
		//首先检查是不是反向代理
		for _, proxy := range conf.Proxies {
			if strings.HasPrefix(url.Path, proxy.Url) {
				remote, err := url.Parse(proxy.Backend)
				if err != nil {
					panic(err)
				}
				reverseProxy := httputil.NewSingleHostReverseProxy(remote)
				originUrl := r.URL.String()
				r.URL.Path = strings.Replace(r.URL.Path, proxy.Url, "", 1)
				reverseProxy.ServeHTTP(w, r)
				log.Printf("[Proxy]%s %s %s -> %s", r.RemoteAddr, r.Method, originUrl, r.URL)
				return
			}
		}

		//其次检查是不是静态文件
		for _, static := range conf.Statics {
			if strings.HasPrefix(url.Path, static.Url) {
				file := filepath.Join(static.Path, strings.Replace(url.Path, static.Url, "", 1))
				ServeFile(w, r, file)
				log.Printf("[StaticFile]%s %s %s -> %s", r.RemoteAddr, r.Method, r.URL, file)
				return
			}
		}

		//都不是，返回404
		http.NotFound(w, r)
		log.Printf("[NotFound]%s %s %s", r.RemoteAddr, r.Method, r.URL)
	}
}

func main() {
	conf, err := LoadConfig("conf.json")
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	} else {
	}

	log.Print("Starting Server: ", fmt.Sprintf("%s:%d", conf.Addr, conf.Port))
	http.HandleFunc("/", genHandler(&conf))
	if err := http.ListenAndServe(fmt.Sprintf("%s:%d", conf.Addr, conf.Port), nil); err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}
